# 🌍 Earthy

Vous aimez lever les yeux et regarder vers le ciel ? Alors vous êtes au bon endroit !

Earthy vous permettra de découvrir une magnifique image de la NASA par jour ainsi que les explications associées. Vous avez aussi la possibilité de sauvegarder l'image pour la définir en fond d'écran. 

<img style="border-radius: 18px;" src="resources/icon.png" alt="Icon Earthy" width="30%">

## 📱  Fonctionnalités

### L’IMAGE DU JOUR DE LA NASA

Earthy permet vous permettra de découvrir une magnifique image de la NASA par jour ainsi que les explications associées. Vous avez aussi la possibilité de sauvegarder l'image pour la définir en fond d’écran.<br>

### SAUVEGARDER VOS LIEUX PRÉFÉRÉS

Earthy vous permettra aussi de sauvegarder très simplement vos lieux préférés pour regarder les étoiles, les planètes, l'ISS et bien d’autres...

### RETENEZ LA DATE DE LA PROCHAINE MISSION

Earthy n'oublie jamais la date et le lieu de la prochaine mission.

## 🚀  API utilisées

[API SpaceX](https://api.spacexdata.com/v3/launches/next) pour récupérer la prochaine mission. ([documentation](https://docs.spacexdata.com/?version=latest))<br><br>

[API NASA](https://api.nasa.gov/planetary/apod?api_key=mCYNbx9NYCh2Ipa8v7s8tTLxoDreDUAGgeAVqnn5) la récupération de l’image du jour ainsi que les explications. ([documentation](https://api.nasa.gov/))<br><br>

[API Google Maps](http://maps.googleapis.com/maps/api/js?key=AIzaSyBKVsUnvnMs8QcCM5xHRxa7JMfTE99ye8k) Javascript afin d'utiliser une carte Google et ajouter des repères sur la carte.<br><br>


## ℹ️  Informations

Si vous avez des questions, possible amélioration, ou encore si vous souhaitez contribuer à ce projet, surtout n'hésitez pas contactez-moi 📧 [Emilien.Chaptard@etu.uca.fr](mailto:emilien.chaptard@etu.uca.fr) avec le sujet "Earthy App".<br>

## 🔨  Bientôt

<ul>
<li>Prise en charge "vidéo du jour" de la NASA</li>
<li>Ajout de la prochaine mission dans le calendrier</li>
<li>Prise en charge de Haptic Touch & 3D Touch</li>
<li>Notification de la prochaine mission</li>
<li>Widget sur iOS</li>
<li>Et bien plus...</li>
</ul><br>


# Exemple

[<img src="https://emchaptard.github.io/data/capture-earthy.png" alt="Screenshot Earthy" width="50%">](https://emchaptard.github.io/data/video-demo.mp4)

🔗 [Voir l'application en vidéo](https://emchaptard.github.io/data/video-demo.mp4).<br>

<br>

# Licence

🔗 [Projet sous 'MIT License'](https://gitlab.com/emilienchaptard/projetangular/-/blob/master/LICENSE).
