import { Component, OnInit } from '@angular/core';
import { ActionSheetController, Platform, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-info',
  templateUrl: './info.page.html',
  styleUrls: ['./info.page.scss'],
})
export class InfoPage implements OnInit {
  constructor(
    public alertController: AlertController,
    public actionCtrl: ActionSheetController,
    private platform: Platform
  )  {
  }
  
  async Merci() {
    const alert = await this.alertController.create({
      header: 'Earthy V.1.1.🌍',
      buttons: [
        {
          text: 'Merci',
          role: 'cancel',
          handler: () => {
            console.log('Confirm Merci');
          }
        }
      ]
    });
    await alert.present();
  }
  ngOnInit() {
  }

}
