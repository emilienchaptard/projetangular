import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { QRcodePage } from './qrcode.page';

describe('QRcodePage', () => {
  let component: QRcodePage;
  let fixture: ComponentFixture<QRcodePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QRcodePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(QRcodePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
