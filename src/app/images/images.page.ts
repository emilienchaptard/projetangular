import { Component, OnInit } from '@angular/core';
import { ImagesService } from '../folder/services/api/images.service';

@Component({
  selector: 'app-images',
  templateUrl: './images.page.html',
  styleUrls: ['./images.page.scss'],
})
export class ImagesPage implements OnInit {
  isload: boolean;
  data: Object;
  show = true;

  constructor(private imagesService: ImagesService) { 
    console.log('page.images');
    this.isload = false;
    setTimeout(() => {
      this.show = false;
    }, 3000);
  }
  
  ngOnInit() {
    this.imagesService.getAPIData().subscribe(_data => {
      console.log("Données JSON : ");
      console.log(_data);
      this.isload = true;
      this.data = _data;
      })
}
}