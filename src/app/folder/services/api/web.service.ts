import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class WebService {

  constructor(private http: HttpClient) {}
  
  getAPIData() {
    return this.http.get("http://maps.googleapis.com/maps/api/js?key=AIzaSyBKVsUnvnMs8QcCM5xHRxa7JMfTE99ye8k");
  }
}