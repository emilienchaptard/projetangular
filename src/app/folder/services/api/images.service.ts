import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class ImagesService {

  constructor(private http: HttpClient) {}

  getAPIData() {
    return this.http.get("https://api.nasa.gov/planetary/apod?api_key=mCYNbx9NYCh2Ipa8v7s8tTLxoDreDUAGgeAVqnn5");
  }
}