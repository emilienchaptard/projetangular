import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

function entierAleatoire(min, max) {
 return Math.floor(Math.random() * (max - min + 0.1)) + min;
}

//La variable contient un nombre aléatoire compris entre 1 et 10
//var lon = entierAleatoire(100, 107);
//var lat = entierAleatoire(1, 5);

@Injectable({
  providedIn: 'root'
})

export class FavoritesService {

  constructor(private http: HttpClient) {}
    

  getAPIData() {
    return this.http.get("https://api.spacexdata.com/v3/launches/next");
    //return this.http.get("https://api.nasa.gov/planetary/earth/imagery/?lon="+lon+"&lat="+lat+"&cloud_score=True&api_key=mCYNbx9NYCh2Ipa8v7s8tTLxoDreDUAGgeAVqnn5");
  }
  
}