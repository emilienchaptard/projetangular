import { Component, OnInit } from '@angular/core';
import { FavoritesService } from '../folder/services/api/favorites.service';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.page.html',
  styleUrls: ['./favorites.page.scss'],
})
export class FavoritesPage implements OnInit {
  isload: boolean;
  data: Object;
  show= true;
  constructor(private favoritesService: FavoritesService) { 
    console.log('page.images');
    this.isload = false;
    setTimeout(() => {
      this.show = false;
    }, 3000);
  }

  ngOnInit() {
    this.favoritesService.getAPIData().subscribe(_data => {
      console.log("Données JSON : ");
      console.log(_data);
      this.isload = true;
      this.data = _data;
      })
}
}