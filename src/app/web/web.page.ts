import { Component, OnInit } from '@angular/core';
import { WebService } from '../folder/services/api/web.service';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsMapTypeId,
  GoogleMapsEvent,
  GoogleMapOptions,
  CameraPosition,
  MarkerOptions,
  Marker,
  Environment
} from '@ionic-native/google-maps';
import { ActionSheetController, Platform, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-web',
  templateUrl: './web.page.html',
  styleUrls: ['./web.page.scss'],
})
export class WebPage implements OnInit {
  isload: boolean;
  data: Object;
  map: GoogleMap;
  constructor(
    public alertController: AlertController,
    public actionCtrl: ActionSheetController,
    private platform: Platform
  ) {
    if (this.platform.is('cordova')) {
      this.loadMap();
    }
  }

  loadMap() {
    Environment.setEnv({
      API_KEY_FOR_BROWSER_RELEASE: 'AIzaSyBKVsUnvnMs8QcCM5xHRxa7JMfTE99ye8k',
      API_KEY_FOR_BROWSER_DEBUG: 'AIzaSyBKVsUnvnMs8QcCM5xHRxa7JMfTE99ye8k'
    });
    this.map = GoogleMaps.create('map_canvas', {
      camera: {
        target: {
          lat: 45.0333,
          lng: 3.8833
        },
        zoom: 15,
        tilt: 30
      }
    });
  }
  placeMarker(markerTitle: string) {
    const marker: Marker = this.map.addMarkerSync({
       title: markerTitle,
       icon: 'red',
       animation: 'DROP',
       position: this.map.getCameraPosition().target
    });
 }
 
 async addMarker() {
  const alert = await this.alertController.create({
    header: 'Ajouter un lieu 🪐💫',
    inputs: [
      {
        name: 'title',
        type: 'text',
        placeholder: 'Titre du lieu'
      }
    ],
    buttons: [
      {
        text: 'Annuler',
        role: 'cancel',
        cssClass: 'secondary',
        handler: () => {
          console.log('Confirm Cancel');
        }
      }, {
        text: 'Ajouter',
        handler: data => {
          console.log('Titre: ' + data.title);
          this.placeMarker(data.title);
        }
      }
    ]
  });
  await alert.present();
}

  ngOnInit() {
    navigator.geolocation.getCurrentPosition(function(position) {
      console.log(position);
  });
  }
}